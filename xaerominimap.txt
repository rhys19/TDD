ignoreUpdate:84
minimap:true
caveMaps:true
showPlayers:true
showMobs:true
showItems:true
showOther:true
showOtherTeam:true
showWaypoints:true
showIngameWaypoints:true
displayRedstone:true
deathpoints:true
oldDeathpoints:true
distance:1
showCoords:true
lockNorth:false
zoom:3
mapSize:2
entityAmount:1
chunkGrid:false
slimeChunks:false
playersColor:15
mobsColor:14
hostileColor:14
itemsColor:12
otherColor:5
mapSafeMode:false
minimapOpacity:100.0
waypointsScale:1.0
antiAliasing:true
blockColours:0
lighting:true
interface::0:36:true:false:false
interface::0:36:true:false:false
interface::0:36:true:false:false
interface::0:36:true:false:false
interface:gui.xaero_minimap:0:0:false:false:false
waypoint:MCA Survival_null:Home:H:-201:67:-68:8:false:0:gui.xaero_default
waypoint:MCA Survival_null:Nether Portal:N:-156:69:-106:5:false:0:gui.xaero_default
waypoint:MCA Survival_null:Quarry:Q:-199:65:-34:6:false:0:gui.xaero_default
waypoint:Hot House Designs_null:Home:H:-119:67:-32:11:false:0:gui.xaero_default
waypoint:MCA Survival_DIM-1:Lava Pump:L:2:34:30:10:false:0:gui.xaero_default
waypoint:MCA Survival_DIM_MYST2:Portal:P:-39:85:-13:1:false:0:gui.xaero_default
waypoint:The Diamond Dimension Let's Play_null:Home:G:-224:91:340:14:false:0:gui.xaero_default
waypoint:The Diamond Dimension Let's Play_null:Mine1:M:-226:57:372:7:false:0:gui.xaero_default
waypoint:The Diamond Dimension Let's Play_null:Bedrock mine:B:-211:11:403:14:false:0:gui.xaero_default
waypoint:The Diamond Dimension Let's Play_null:gui.xaero_deathpoint:D:-187:65:327:0:false:1:gui.xaero_default
key_gui.xaero_toggle_map:35
key_gui.xaero_enlarge_map:44
key_gui.xaero_waypoints_key:22
key_gui.xaero_zoom_in:23
key_gui.xaero_zoom_out:24
key_gui.xaero_minimap_settings:21
key_gui.xaero_new_waypoint:48
